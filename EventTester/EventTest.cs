﻿/* Written by David Pena
 * Updated 12/02/16
 * 4 Tab Stops
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeSpanAlgorithmTester
{
    public class EventLine
    {
        #region GlobalInitializers

        public DateTime alpha;
        public DateTime beta;
        Random num = new Random(Guid.NewGuid().GetHashCode());

        #endregion

        #region EventLineConstructors

        /// <summary>
        /// create an event line that has a start time and end time where 
        /// start is Now - time and end it Now + time.
        /// </summary>
        /// <param name="time">Total span where time is a positive decimal 
        /// of days</param>
        public EventLine(float time)
        {
            // get current time
            DateTime current = DateTime.Now;

            // convert time to total number of seconds.
            double seconds = time * 86400.0;

            // set start time by subtracting number of seconds from current time.
            alpha = current.AddSeconds(-seconds);

            // set end time by adding number of seconds to current time.
            beta = current.AddSeconds(seconds);
        }

        /// <summary>
        /// create on event on an event line where the the event could start 
        /// and end at any point on the event line.
        /// </summary>
        /// <param name="a">Total span where time is a positive decimal of 
        /// days</param>
        /// <param name="b">Total duration of event where timeEvnt is a 
        /// positive decimal of hours</param>
        public EventLine(bool useRandom, float timeSpan, float timeEvnt)
        {
            // get current time
            DateTime current = DateTime.Now;

            // convert timeSpan, and timeEvnt to total number of seconds.
            double secondsSpan = timeSpan * 86400.0;
            double secondsEvnt = timeEvnt * 3600.0;

            // drive alpha and beta
            alpha = current.AddSeconds(-secondsSpan);
            beta = current.AddSeconds(secondsSpan);

            // if useRandom is true, than make random doubles to make a random span
            if (useRandom)
            {
                // set start point of event, which is somewhere between alpha
                // and beta
                alpha = alpha.AddSeconds(num.NextDouble() * (2.0 * secondsSpan));
                // set end point of event, which is somewhere after startpoint of 
                // event, and before or equal to total event time.
                beta = alpha.AddSeconds(num.NextDouble() * secondsEvnt);
            }

            // if useRandom is false, then use static double to make static span
            else
            {
                alpha = alpha.AddSeconds(600);
                beta = alpha.AddSeconds(secondsEvnt);
            }
        }

        #endregion

        #region OperatorOverload
        /// <summary>
        /// checks that two operands are the same.
        /// </summary>
        /// <param name="a">The first operand</param>
        /// <param name="b">The second operand</param>
        /// <returns>false if they are not the same,
        /// or if one or both operands are null.</returns>
        public static bool operator ==(EventLine a, EventLine b)
        {
            //test that EventLine a and EventLine b are not null
            if (object.ReferenceEquals(a, null) || object.ReferenceEquals(b, null))
                return false;

            //return true;
            return (a.alpha.Equals(b.alpha) && a.beta.Equals(b.beta));
        }

        /// <summary>
        /// checks that two operands are not the same
        /// </summary>
        /// <param name="a">the first operand</param>
        /// <param name="b">the second operand</param>
        /// <returns>false if they are the same, or true
        /// if they are not the same, or one or both are null</returns>
        public static bool operator !=(EventLine a, EventLine b)
        {
            //test that EventLine a is equal to EventLine b
            return !(a == b);
        }

        #endregion

        #region overrideToString
        /// <summary>
        /// print out the start time and end time of an event
        /// </summary>
        /// <returns>whether or not it works</returns>
        public override string ToString()
        {
            return "Start time: " + alpha.ToString() + " --> End time: " + beta.ToString();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #endregion
    }
}
