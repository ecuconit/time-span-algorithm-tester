﻿/* Written by David Pena
 * Updated 12/02/16
 * 4 Tab Stops
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace TimeSpanAlgorithmTester
{
    public partial class TimeSpanAlgorithmTesterConsole : Form
    {
        #region Initializers

        private static int maxLength = 1000000;
        private static int maxRounds = 10000;
        private Color green = Color.Green;
        private Color red = Color.Red;
        private Color black = Color.Black;
        private Random num = new Random(Guid.NewGuid().GetHashCode());
        //public static string StartupPath { get; }

        #endregion

        #region Public Section

        public TimeSpanAlgorithmTesterConsole()
        {
            InitializeComponent();

            DateTime start = DateTime.Now;
            bool result = true;

            #region passedTests
            ////Test that debug is working
            //result = result && Test1();
            //Test2();
            //Test3();
            //result = result && Test4();
            //result = result && Test5();

            ////Test that span is working
            //Test6();
            //Test7();

            ////Test validEvent
            //result = result && test8();
            //result = result && test9();
            //result = result && test10();
            //result = result && test11();
            //result = result && test12();
            //result = result && test13();

            ////Test validEventPrime
            //result = result && test14();
            //result = result && test15();
            //result = result && test16();
            //result = result && test17();
            //result = result && test18();
            //result = result && test19();

            ////Test validEventDoubPrime
            //result = result && test20();
            //result = result && test21();
            //result = result && test22();
            //result = result && test23();
            //result = result && test24();
            //result = result && test25();

            ////Test validEvent and ValidEventPrime return same results
            EventLine timeline = new EventLine(5);
            List<EventLine> eventList = setNewEventList(7, 6);
            result = result && testEqualAll(timeline, eventList);
            //testSpeedVE(timeline, eventList);
            //testSpeedVEP(timeline, eventList);
            //testSpeedVEDP(timeline, eventList);
            #endregion

            result = result && testPerformance();

            //ToDebug(result, "Result of all of the tests are:  " + (result ? "Successful" : "Failure"));
            //ToDebug("Total runtime is: " + (DateTime.Now - start).TotalSeconds + " seconds");
        }

        #endregion

        #region DebugTests

        /// <summary>
        /// Test that debug text box is working
        /// </summary>
        /// <returns>Whether or not it works</returns>
        private bool Test1()
        {
            ToDebug("Starting Test 1: ", black);

            // initialize a simple message
            string simpleMessage = "Hello World\n";

            // Test to put message into display
            this.TimeSpanAlgorithmTestBox.Text = simpleMessage;

            return (this.TimeSpanAlgorithmTestBox.Text == simpleMessage);
        }

        /// <summary>
        /// Test overloaded ToDebug Works on a fail message.
        /// Message should print out in red.
        /// </summary>
        private void Test2()
        {
            ToDebug("Starting Test 2: ", black);

            // initialize a string
            string test = "This should be red";

            ToDebug(false, test);
        }

        /// <summary>
        /// Test overloaded ToDebug works on a pass message.
        /// Message should print out in green.
        /// </summary>
        private void Test3()
        {
            ToDebug("Starting Test 3: ", black);

            // initialize a string
            string test = "This should be green";

            ToDebug(true, test);
        }

        /// <summary>
        /// Test that EventLine can be constructed
        /// </summary>
        /// <returns>whether or not its true</returns>
        private bool Test4()
        {
            ToDebug("Starting Test 4: ", black);

            // Initialize a new EventLine
            EventLine temp = new EventLine(1);

            // Check that temp is not empty
            if (temp != null) 
                ToDebug("The event was created", black);

            return (temp != null);
        }

        /// <summary>
        /// Test overloaded EventLine can be constructed
        /// </summary>
        /// <returns>Whether or not it works</returns>
        private bool Test5()
        {
            ToDebug("Starting Test 5: ", black);

            // Initialize a new overloaded EventLine
            EventLine temp = new EventLine(true, 2, 3);

            // Check that temp is not empty
            if (temp != null) 
                ToDebug("The overloaded event was created", black);

            return (temp != null);
        }
        
        #endregion

        #region Span Test

        /// <summary>
        /// Test that new EventLine is created with static alpha, and beta times.
        /// </summary>
        private void Test6()
        {
            ToDebug("Starting Test 6: ", black);

            //Create new event line spanning 4 hours.
            EventLine temp = new EventLine(2);

            //Print time of eventline
            ToDebug(temp.ToString());
        } 

        /// <summary>
        /// Test overloaded EventLine with two different spans.
        /// </summary>
        private void Test7()
        {
            ToDebug("Starting Test 7: ", black);

            //Create new event line spanning 8 hours, and create an event that
            //starts within this line.
            EventLine temp = new EventLine(true, 4, 4);

            //Print start and end of created event.
            ToDebug(temp.ToString());
        }

        #endregion

        # region Test validEvent

        /// <summary>
        /// Use validEvent to test that an event is valid if it is inside an event time.
        /// </summary>
        /// <returns>whether or not it works</returns>
        private bool test8()
        {
            ToDebug("Starting Test 8: ", black);

            //Create new EventLine to test against
            EventLine temp = new EventLine(4);

            //Create new event that is inside temp EventLine
            EventLine evntTemp = new EventLine(false, 2, 2);

            //Print out EventLine
            ToDebug(temp.ToString());

            //Print out event
            ToDebug(validEvent(temp, evntTemp), evntTemp.ToString());

            return validEvent(temp, evntTemp);
        }

        /// <summary>
        /// Use validEvent to test that an event is valid if it starts inside the EventLine
        /// and ends outside the EventLine.
        /// </summary>
        /// <returns>Whether or not it works</returns>
        private bool test9()
        {
            ToDebug("Starting Test 9: ", black);

            //Create new EventLine to test against
            EventLine temp = new EventLine(2);

            //Create new event that starts inside temp EventLine and ends outside temp EventLine
            EventLine evntTemp = new EventLine(false, 0, 96);

            //Print out EventLine
            ToDebug(temp.ToString());

            //Print out event
            ToDebug(validEvent(temp, evntTemp), evntTemp.ToString());

            return validEvent(temp, evntTemp);
        }

        /// <summary>
        /// Use validEvent to test that an event is valid if it starts outside the EventLine
        /// and ends inside the EventLine
        /// </summary>
        /// <returns>Whether or not it is true</returns>
        private bool test10()
        {
            ToDebug("Starting Test 10: ", black);

            //Create new EventLine to test against
            EventLine temp = new EventLine(2);

            //Create new event that starts outsid temp EventLine and ends inside temp EventLine
            EventLine evntTemp = new EventLine(false, 4, 96);

            //Print out EventLine
            ToDebug(temp.ToString());

            //Print out event
            ToDebug(validEvent(temp, evntTemp), evntTemp.ToString());

            return validEvent(temp, evntTemp);
        }

        /// <summary>
        /// Use validEvent to test that an event is valid when it starts before the EventLine,
        /// and ends after the EventLine.
        /// </summary>
        /// <returns>Whether or not it is true</returns>
        private bool test11()
        {
            ToDebug("Starting Test 11: ", black);

            //Create new EventLine to test against
            EventLine temp = new EventLine(2);

            //Create new event that starts before temp EventLine, and ends after temp EventLine
            EventLine evntTemp = new EventLine(false, 3, 140);

            //Print out EventLine
            ToDebug(temp.ToString());

            //Print out event
            ToDebug(validEvent(temp, evntTemp), evntTemp.ToString());

            return validEvent(temp, evntTemp);
        }

        /// <summary>
        /// Use validEvent to test that an event is valid when it starts and ends before
        /// temp EventLine start.
        /// </summary>
        /// <returns>Whether or not it works</returns>
        private bool test12()
        {
            ToDebug("Starting Test 12: ", black);

            //Create new EventLine to test against
            EventLine temp = new EventLine(2);

            //Create new event that starts and ends before temp EventLine.
            EventLine evntTemp = new EventLine(false, 4, 1);

            //Print out EventLine
            ToDebug(temp.ToString());

            //Print out event
            ToDebug(!validEvent(temp, evntTemp), evntTemp.ToString());

            return !validEvent(temp, evntTemp);
        }

        /// <summary>
        /// Use validEvent to test that an event is valid when it starts and ends after
        /// temp EventLine start.
        /// </summary>
        /// <returns>Whether or not it works</returns>
        private bool test13()
        {
            ToDebug("Starting Test 13: ", black);

            //Create new EventLine to test against
            EventLine temp = new EventLine(1);

            //Create new event that starts and ends after temp EventLine.
            EventLine evntTemp = new EventLine(false, -4, 3);

            //Print out EventLine
            ToDebug(temp.ToString());

            //Print out event
            ToDebug(!validEvent(temp, evntTemp), evntTemp.ToString());

            return !validEvent(temp, evntTemp);
        }

        #endregion

        #region Test validEventPrime

        /// <summary>
        /// Use validEventPrime to test that an event is valid if it is inside an event time.
        /// </summary>
        /// <returns>whether or not it works</returns>
        private bool test14()
        {
            ToDebug("Starting Test 14: ", black);

            //Create new EventLine to test against
            EventLine temp = new EventLine(4);

            //Create new event that is inside temp EventLine
            EventLine evntTemp = new EventLine(false, 2, 2);

            //Print out EventLine
            ToDebug(temp.ToString());

            //Print out event
            ToDebug(validEventPrime(temp, evntTemp), evntTemp.ToString());

            return validEventPrime(temp, evntTemp);
        }

        /// <summary>
        /// Use validEventPrime to test that an event is valid if it starts inside the EventLine
        /// and ends outside the EventLine.
        /// </summary>
        /// <returns>Whether or not it works</returns>
        private bool test15()
        {
            ToDebug("Starting Test 15: ", black);

            //Create new EventLine to test against
            EventLine temp = new EventLine(2);

            //Create new event that starts inside temp EventLine and ends outside temp EventLine
            EventLine evntTemp = new EventLine(false, 0, 96);

            //Print out EventLine
            ToDebug(temp.ToString());

            //Print out event
            ToDebug(validEventPrime(temp, evntTemp), evntTemp.ToString());

            return validEventPrime(temp, evntTemp);
        }

        /// <summary>
        /// Use validEventPrime to test that an event is valid if it starts outside the EventLine
        /// and ends inside the EventLine
        /// </summary>
        /// <returns>Whether or not it is true</returns>
        private bool test16()
        {
            ToDebug("Starting Test 16: ", black);

            //Create new EventLine to test against
            EventLine temp = new EventLine(2);

            //Create new event that starts outsid temp EventLine and ends inside temp EventLine
            EventLine evntTemp = new EventLine(false, 4, 96);

            //Print out EventLine
            ToDebug(temp.ToString());

            //Print out event
            ToDebug(validEventPrime(temp, evntTemp), evntTemp.ToString());

            return validEventPrime(temp, evntTemp);
        }

        /// <summary>
        /// Use validEventPrime to test that an event is valid when it starts before the EventLine,
        /// and ends after the EventLine.
        /// </summary>
        /// <returns>Whether or not it is true</returns>
        private bool test17()
        {
            ToDebug("Starting Test 17: ", black);

            //Create new EventLine to test against
            EventLine temp = new EventLine(2);

            //Create new event that starts before temp EventLine, and ends after temp EventLine
            EventLine evntTemp = new EventLine(false, 3, 140);

            //Print out EventLine
            ToDebug(temp.ToString());

            //Print out event
            ToDebug(validEventPrime(temp, evntTemp), evntTemp.ToString());

            return validEventPrime(temp, evntTemp);
        }

        /// <summary>
        /// Use validEventPrime to test that an event is valid when it starts and ends before
        /// temp EventLine start.
        /// </summary>
        /// <returns>Whether or not it works</returns>
        private bool test18()
        {
            ToDebug("Starting Test 18: ", black);

            //Create new EventLine to test against
            EventLine temp = new EventLine(2);

            //Create new event that starts and ends before temp EventLine.
            EventLine evntTemp = new EventLine(false, 4, 1);

            //Print out EventLine
            ToDebug(temp.ToString());

            //Print out event
            ToDebug(!validEventPrime(temp, evntTemp), evntTemp.ToString());

            return !validEventPrime(temp, evntTemp);
        }

        /// <summary>
        /// Use validEventPrime to test that an event is valid when it starts and ends after
        /// temp EventLine start.
        /// </summary>
        /// <returns>Whether or not it works</returns>
        private bool test19()
        {
            ToDebug("Starting Test 19: ", black);

            //Create new EventLine to test against
            EventLine temp = new EventLine(1);

            //Create new event that starts and ends after temp EventLine.
            EventLine evntTemp = new EventLine(false, -4, 3);

            //Print out EventLine
            ToDebug(temp.ToString());

            //Print out event
            ToDebug(!validEventPrime(temp, evntTemp), evntTemp.ToString());

            return !validEventPrime(temp, evntTemp);
        }
        #endregion

        #region Test validEventDoubPrime

        /// <summary>
        /// Use validEventDoubPrime to test that an event is valid if it is inside an event time.
        /// </summary>
        /// <returns>whether or not it works</returns>
        private bool test20()
        {
            ToDebug("Starting Test 20: ", black);

            //Create new EventLine to test against
            EventLine temp = new EventLine(4);

            //Create new event that is inside temp EventLine
            EventLine evntTemp = new EventLine(false, 2, 2);

            //Print out EventLine
            ToDebug(temp.ToString());

            //Print out event
            ToDebug(validEventDoubPrime(temp, evntTemp), evntTemp.ToString());

            return validEventDoubPrime(temp, evntTemp);
        }

        /// <summary>
        /// Use validEventDoubPrime to test that an event is valid if it starts inside the EventLine
        /// and ends outside the EventLine.
        /// </summary>
        /// <returns>Whether or not it works</returns>
        private bool test21()
        {
            ToDebug("Starting Test 21: ", black);

            //Create new EventLine to test against
            EventLine temp = new EventLine(2);

            //Create new event that starts inside temp EventLine and ends outside temp EventLine
            EventLine evntTemp = new EventLine(false, 0, 96);

            //Print out EventLine
            ToDebug(temp.ToString());

            //Print out event
            ToDebug(validEventDoubPrime(temp, evntTemp), evntTemp.ToString());

            return validEventDoubPrime(temp, evntTemp);
        }

        /// <summary>
        /// Use validEventDoubPrime to test that an event is valid if it starts outside the EventLine
        /// and ends inside the EventLine
        /// </summary>
        /// <returns>Whether or not it is true</returns>
        private bool test22()
        {
            ToDebug("Starting Test 22: ", black);

            //Create new EventLine to test against
            EventLine temp = new EventLine(2);

            //Create new event that starts outsid temp EventLine and ends inside temp EventLine
            EventLine evntTemp = new EventLine(false, 4, 96);

            //Print out EventLine
            ToDebug(temp.ToString());

            //Print out event
            ToDebug(validEventDoubPrime(temp, evntTemp), evntTemp.ToString());

            return validEventDoubPrime(temp, evntTemp);
        }

        /// <summary>
        /// Use validEventDoubPrime to test that an event is valid when it starts before the EventLine,
        /// and ends after the EventLine.
        /// </summary>
        /// <returns>Whether or not it is true</returns>
        private bool test23()
        {
            ToDebug("Starting Test 23: ", black);

            //Create new EventLine to test against
            EventLine temp = new EventLine(2);

            //Create new event that starts before temp EventLine, and ends after temp EventLine
            EventLine evntTemp = new EventLine(false, 3, 140);

            //Print out EventLine
            ToDebug(temp.ToString());

            //Print out event
            ToDebug(validEventDoubPrime(temp, evntTemp), evntTemp.ToString());

            return validEventDoubPrime(temp, evntTemp);
        }

        /// <summary>
        /// Use validEventDoubPrime to test that an event is valid when it starts and ends before
        /// temp EventLine start.
        /// </summary>
        /// <returns>Whether or not it works</returns>
        private bool test24()
        {
            ToDebug("Starting Test 24: ", black);

            //Create new EventLine to test against
            EventLine temp = new EventLine(2);

            //Create new event that starts and ends before temp EventLine.
            EventLine evntTemp = new EventLine(false, 4, 1);

            //Print out EventLine
            ToDebug(temp.ToString());

            //Print out event
            ToDebug(!validEventDoubPrime(temp, evntTemp), evntTemp.ToString());

            return !validEventDoubPrime(temp, evntTemp);
        }

        /// <summary>
        /// Use validEventDoubPrime to test that an event is valid when it starts and ends after
        /// temp EventLine start.
        /// </summary>
        /// <returns>Whether or not it works</returns>
        private bool test25()
        {
            ToDebug("Starting Test 25: ", black);

            //Create new EventLine to test against
            EventLine temp = new EventLine(1);

            //Create new event that starts and ends after temp EventLine.
            EventLine evntTemp = new EventLine(false, -4, 3);

            //Print out EventLine
            ToDebug(temp.ToString());

            //Print out event
            ToDebug(!validEventDoubPrime(temp, evntTemp), evntTemp.ToString());

            return !validEventDoubPrime(temp, evntTemp);
        }
        #endregion

        #region validEventEqual
        /// <summary>
        /// Test that a number of checked events are equal to each other.
        /// </summary>
        /// <returns>True if all events are equal to each other</returns>
        private bool testEqualAll(EventLine timeline, List<EventLine> eventList)
        {
            ToDebug("Starting Test testEqualAll: ", black);

            // boolean that will return true if all events are equal
            bool worked = true;

            // check that each item in eventList gets the same results in both
            // validEvent and validEventPrime
            foreach(EventLine span in eventList)
            {
                //ToDebug(span.ToString());
                worked = worked && (validEvent(timeline, span) == validEventPrime(timeline, span)) && (validEvent(timeline, span) == validEventPrime(timeline, span));

                // print out any failed comparisons
                if (!worked)
                {
                    ToDebug(validEvent(timeline, span), span.ToString());
                    ToDebug(validEventPrime(timeline, span), span.ToString());
                    ToDebug(validEventDoubPrime(timeline, span), span.ToString());
                }
            }
            ToDebug(worked, (worked ? "validEvent, validEventPrime, and validEventDoubPrime get the same result on " + eventList.Count() + "  different items." : "validEvent, validEventPrime, and validEventDoubPrime did not get same result"));
            ToDebug("\r\n");
            return worked;
        }

        #endregion

        #region Time of validEvent and validEventPrime
        /// <summary>
        /// This test will give the total runtime of validEvent
        /// </summary>
        /// <param name="spanTime">total span time in positive integer of days</param>
        /// <param name="days">allowed event period in positive integer of days</param>
        /// <param name="hours">max event duration in positive integer of hours</param>
        private void testSpeedVE(EventLine timeline, List<EventLine> eventList)
        {
            ToDebug("Starting Test testSpeedVE: ", black);

            // get current time before using validEvent
            DateTime startVE = DateTime.Now;

            // check that event is valid or not
            foreach (EventLine span in eventList)
                validEvent(timeline, span);

            //print out total to run validEvent
            ToDebug("Total runtime of ValidEvent is: " + (DateTime.Now - startVE).TotalSeconds.ToString() + " Seconds");
        }

        /// <summary>
        /// This test will give the total runtime of validEventPrime
        /// </summary>
        /// <param name="spanTime">total span time in positive integer of days</param>
        /// <param name="days">allowed event period in positive integer of days</param>
        /// <param name="hours">max event duration in positive integer of hours</param>
        private void testSpeedVEP(EventLine timeline, List<EventLine> eventList)
        {
            ToDebug("Starting Test testSpeedVEP: ", black);

            // get current time before using validEventPrime
            DateTime startVE = DateTime.Now;

            // check that event is valid or not
            foreach (EventLine span in eventList)
                validEventPrime(timeline, span);

            //print out total time of test
            ToDebug("Total runtime of ValidEventPrime is: " + (DateTime.Now - startVE).TotalSeconds.ToString() + " Seconds");
        }

        /// <summary>
        /// This test will give the total runtime of validEventDoubPrime
        /// </summary>
        /// <param name="spanTime">total span time in positive integer of days</param>
        /// <param name="days">allowed event period in positive integer of days</param>
        /// <param name="hours">max event duration in positive integer of hours</param>
        private void testSpeedVEDP(EventLine timeline, List<EventLine> eventList)
        {
            ToDebug("Starting Test testSpeedVEP: ", black);

            // get current time before using validEventPrime
            DateTime startVE = DateTime.Now;

            // check that event is valid or not
            foreach (EventLine span in eventList)
                validEventDoubPrime(timeline, span);

            //print out total time of test
            ToDebug("Total runtime of ValidEventDoubPrime is: " + (DateTime.Now - startVE).TotalSeconds.ToString() + " Seconds");
        }

        #endregion

        #region Preformance Test
        /// <summary>
        /// This test will save the time to complete validEvent in a list of validEventTimes
        /// and save the time to complete validEventPrime in a list of ValidEventPrimeTimes
        /// </summary>
        /// <returns>whether or not it works</returns>
        private bool testPerformance()
        {
            List<double> validEventTimes = new List<double>();
            List<double> validEventPrimeTimes = new List<double>();
            List<double> validEventDoubPrimeTimes = new List<double>();
            for (int x = 0; x < maxRounds; x++)
            {
                // make a new timeline to test events against, using a random number of days.
                // days can be between 1 and 32
                EventLine timeline = new EventLine(num.Next(1, 32));

                // make a new list of events each time, generating a random number of days, and total hours.
                List<EventLine> events = setNewEventList(num.Next(1, 32), num.Next(1, 745));

                //get the total time to check the array of new events on valid events.
                #region validEventCheck
                // get current time before using validEvent
                DateTime startVE = DateTime.Now;

                // check that event is valid or not
                foreach (EventLine span in events)
                    validEvent(timeline, span);

                //save the completion time into validEventsTimes list
                validEventTimes.Add((DateTime.Now - startVE).TotalSeconds);
                #endregion

                //get the total time to check the array of new events on valid events Prime.
                #region validEventPrimeCheck
                // get current time before using validEventPrime
                startVE = DateTime.Now;

                // check that event is valid or not
                foreach (EventLine span in events)
                    validEventPrime(timeline, span);

                //save the completion time into validEventsPrimeTimes list
                validEventPrimeTimes.Add((DateTime.Now - startVE).TotalSeconds);
                #endregion

                //get the total time to check the array of new events on validEventsDoubPrime.
                #region validEventDoubPrimeCheck
                // get current time before using validEventPrime
                startVE = DateTime.Now;

                // check that event is valid or not
                foreach (EventLine span in events)
                    validEventDoubPrime(timeline, span);

                //save the completion time into validEventsPrimeTimes list
                validEventDoubPrimeTimes.Add((DateTime.Now - startVE).TotalSeconds);
                #endregion

            }

            // if validEventsTimes, and validEventsPrimeTimes do not have a size equal
            // to maxRounds, the test fails.
            if (((validEventPrimeTimes.Count != maxRounds) && (validEventTimes.Count != maxRounds)) && ((validEventTimes.Count != maxRounds) && (validEventDoubPrimeTimes.Count != maxRounds)))
                return false;

            // print out the preformance results of validEvents, and validEventsPrime
            // the test passes.
            else
            {
                performanceResults(validEventTimes, validEventPrimeTimes, validEventDoubPrimeTimes);
                return true;
            }
        }

        /// <summary>
        /// Prints out the overall preformance of validEvents, and validEventsPrime
        /// </summary>
        /// <param name="validEventsTimes">List of maxRounds length of completion times on validEvents with maxLength number of items</param>
        /// <param name="validEventsPrimeTimes">List of maxRounds length of completion times on validEventsPrime with maxLength number of items</param>
        private void performanceResults(List<double> validEventTimes, List<double> validEventPrimeTimes, List<double> validEventDoubPrimeTimes)
        {
            // Print out statistiacal analysis of validEvents
            ToDebug("Statistical Analysis of validEvent", black);
            ToDebug("The total time to finish all validEventTimes was: " + validEventTimes.Sum() + " seconds");
            ToDebug("The fastest time for validEventTimes was: " + validEventTimes.Min() + " seconds");
            ToDebug("The slowest time for validEventTimes was: " + validEventTimes.Max() + " seconds");
            ToDebug("The average time for validEventTimes was: " + validEventTimes.Average() + " seconds");
            ToDebug("The standard deviation for validEventTimes was: " + CalculateStdDev(validEventTimes));
            ToDebug("\r\n");

            // Print out statistical analysis of validEventsPrime
            ToDebug("Statistical Analysis of validEventPrime", black);
            ToDebug("The total time to finish all validEventPrimeTimes was: " + validEventPrimeTimes.Sum() + " seconds");
            ToDebug("The fastest time for validEventPrimeTimes was: " + validEventPrimeTimes.Min() + " seconds");
            ToDebug("The slowest time for validEventPrimeTimes was: " + validEventPrimeTimes.Max() + " seconds");
            ToDebug("The average time for validEventPrimeTimes was: " + validEventPrimeTimes.Average() + " seconds");
            ToDebug("The standard deviation for validEventPrimeTimes was: " + CalculateStdDev(validEventPrimeTimes));
            ToDebug("\r\n");

            // Print out statistical analysis of validEventsPrime
            ToDebug("Statistical Analysis of validEventDoubPrime", black);
            ToDebug("The total time to finish all validEventDoubPrimeTimes was: " + validEventDoubPrimeTimes.Sum() + " seconds");
            ToDebug("The fastest time for validEventDoubPrimeTimes was: " + validEventDoubPrimeTimes.Min() + " seconds");
            ToDebug("The slowest time for validEventDoubPrimeTimes was: " + validEventDoubPrimeTimes.Max() + " seconds");
            ToDebug("The average time for validEventDoubPrimeTimes was: " + validEventDoubPrimeTimes.Average() + " seconds");
            ToDebug("The standard deviation for validEventDoubPrimeTimes was: " + CalculateStdDev(validEventDoubPrimeTimes));
            ToDebug("\r\n");
        }

        /// <summary>
        /// Calculates the standard deviation on a list
        /// </summary>
        /// <param name="values">List of positive doubles from either validEvents, or validEventsPrime</param>
        /// <returns>Standard deviation of list</returns>
        private double CalculateStdDev(List<double> values)
        {
            double ret = 0;

            if (values.Count() > 0)
            {
                //Compute the Average      
                double avg = values.Average();
                //Perform the Sum of (value-avg)_2_2      
                double sum = values.Sum(d => Math.Pow(d - avg, 2));
                //Put it all together      
                ret = Math.Sqrt((sum) / (values.Count() - 1));
            }

            return ret;
        }
        #endregion

        #region Initialize new eventList
        /// <summary>
        /// Makes a new list of events based on days and hours
        /// </summary>
        /// <param name="days">Postive integer of max days in events</param>
        /// <param name="hours">Positive integer of max hours event can last</param>
        /// <returns>new event list</returns>
        private List<EventLine> setNewEventList(float days, float hours)
        {
            // Initialize a new eventlist
            List<EventLine> eventList = new List<EventLine>();

            // Add a new event to eventList until it has reached
            // maxLength
            for (int x = 1; x <= maxLength; x++)
                eventList.Add(new EventLine(true, days, hours));

            return eventList;
        }
        #endregion

        #region ValidEvent

        /// <summary>
        /// Check if an event is valid on the event timeline using the original method.
        /// The event is valid if any of these conditions are true.
        /// 1. x > alpha && x < beta
        /// 2. y > alpha && y < beta
        /// 3. x < alpha && y > beta
        /// </summary>
        /// <param name="timeline">timeline where events can be valid</param>
        /// <param name="evnt">user made event</param>
        /// <returns>true if event occurs on timeline</returns>
        private bool validEvent(EventLine timeline, EventLine evnt)
        {
            return (((evnt.alpha > timeline.alpha) && (evnt.alpha < timeline.beta)) ||
                    ((evnt.beta > timeline.alpha)  && (evnt.beta < timeline.beta))  ||
                    ((evnt.alpha < timeline.alpha) && (evnt.beta > timeline.beta)));
        }

        /// <summary>
        /// Check if event is valid on the event timeline using the new method.
        /// The event is valid if the following condition is true:
        /// y > alpha && x < beta
        /// </summary>
        /// <param name="timeline"></param>
        /// <param name="evnt"></param>
        /// <returns></returns>
        private bool validEventPrime(EventLine timeline, EventLine evnt)
        {
            return ((evnt.beta > timeline.alpha) && (evnt.alpha < timeline.beta));
        }

        /// <summary>
        /// Check if event is valid on the event timeline using the new method.
        /// The event is valid if the following condition is true:
        /// x < beta && y > alpha
        /// </summary>
        /// <param name="timeline"></param>
        /// <param name="evnt"></param>
        /// <returns></returns>
        private bool validEventDoubPrime(EventLine timeline, EventLine evnt)
        {
            return ((evnt.alpha < timeline.beta) && (evnt.beta > timeline.alpha));
        }

        #endregion

        #region ToDebug

        /// <summary>
        /// Writes success message to debug
        /// </summary>
        /// <param name="message">the message to print to debug</param>
        private void ToDebug(string message)
        {
            if (message == null) this.TimeSpanAlgorithmTestBox.Text = "\r\n";
            else ToDebug(true, message);
        }

        /// <summary>
        /// Writes colored message to debug. Green if the message was a success. Red if the
        /// message was a fail.
        /// </summary>
        /// <param name="worked">boolean of pass or fail</param>
        /// <param name="message">the message to print to debug</param>
        private void ToDebug(bool worked, string message)
        {
            ToDebug(message, (worked ? green : red));
        }

        /// <summary>
        /// Prints message to debug, with a specified color
        /// </summary>
        /// <param name="message">message to print to debug</param>
        /// <param name="color">color the message needs to be</param>
        private void ToDebug(string message, Color color)
        {
            this.TimeSpanAlgorithmTestBox.AppendText(message + "\r\n", color);
        }

        #endregion

    }
}
