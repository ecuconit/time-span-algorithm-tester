﻿namespace TimeSpanAlgorithmTester
{
    partial class TimeSpanAlgorithmTesterConsole
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TimeSpanAlgorithmTestBox = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // TimeSpanAlgorithmTestBox
            // 
            this.TimeSpanAlgorithmTestBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TimeSpanAlgorithmTestBox.Location = new System.Drawing.Point(0, 0);
            this.TimeSpanAlgorithmTestBox.Name = "TimeSpanAlgorithmTestBox";
            this.TimeSpanAlgorithmTestBox.Size = new System.Drawing.Size(571, 565);
            this.TimeSpanAlgorithmTestBox.TabIndex = 0;
            this.TimeSpanAlgorithmTestBox.Text = "";
            // 
            // TimeSpanAlgorithmTesterConsole
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(571, 565);
            this.Controls.Add(this.TimeSpanAlgorithmTestBox);
            this.Name = "TimeSpanAlgorithmTesterConsole";
            this.Text = "TimeSpanAlgorithmTesterText";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox TimeSpanAlgorithmTestBox;

    }
}

